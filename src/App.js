import './App.css';
import React, {useState} from 'react'
import styled from 'styled-components'

const DivWrapper = styled.div`
height: ${props => props.height ? `${props.height}vh` : '10vh' };
width:70vw;
background-color: yellow;
position:relative;
`;

const MainContainer = styled.div`
 

`;



const LineWrapper = styled.div`
  position:absolute;
  left: ${props => props.linePosition ?  `${props.linePosition}px` : '0px'};
  background-color: red;
  height: 100%;
  width: 0.5%;
`;


function App() {

  const [height, setHeight] = useState(10);
  const [linePosition, setLinePosition] = useState(0);

  return (
    <MainContainer>
       <DivWrapper onClick={(event) => {
          console.log(event, event.clientY, linePosition);

          if(event.clientX < linePosition){
            setHeight(height - 10);
            setLinePosition(linePosition - 30)
          }else{
            setHeight(height + 10);
            setLinePosition(linePosition + 30)
          }
        
       }} height={height}>
        <LineWrapper linePosition={linePosition} />
      </DivWrapper>
    </MainContainer>
   
  );
}

export default App;
